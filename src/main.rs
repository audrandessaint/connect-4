#![allow(dead_code)]

use std::io;
use std::cmp;
use std::vec::Vec;

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
enum Color {
    Blue,
    Red,
}

impl Color {
    fn other(&self) -> Color {
        match self {
            Color::Blue => Color::Red,
            Color::Red => Color::Blue,
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
enum Cell {
    Empty,
    Occupied(Color),
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
struct Board {
    cells: Vec<Cell>,
    width: usize,
    height: usize,
    number_tokens: u32,
}

impl Board {
    fn new() -> Self {
        Self {
            cells: vec![Cell::Empty; 42],
            width: 7,
            height: 6,
            number_tokens: 0,
        }
    }

    fn get_cell(&self, i: usize) -> &Cell {
        &self.cells[i]
    }

    fn get_column_height(&self, column: usize) -> usize {
        let mut counter: usize = 0;
        for i in 0..self.height {
            if let Cell::Empty = self.cells[column * self.height + i] {
                return counter;
            }
            counter += 1;
        }
        return counter;
    }

    fn get_possible_columns(&self) -> Vec<usize> {
        let mut result = Vec::<usize>::new();
        for i in 0..self.width {
            if let Cell::Empty = self.cells[i * self.height + self.height - 1] {
                result.push(i);
            }
        }
        return result;
    }
    
    fn is_full(&self) -> bool {
        self.number_tokens == (self.width * self.height).try_into().unwrap()
    }

    fn get_column_indices(&self, column: usize) -> Vec<usize> {
        (0..self.height).map(|row| column * self.height + row).collect()
    }

    fn get_row_indices(&self, row: usize) -> Vec<usize> {
        (0..self.width).map(|column| column * self.height + row).collect()
    }

    // WARNING: works only if height < width
    fn get_diagonal_indices(&self, column: usize, row: usize, up: bool) -> Vec<usize> {
        if up {
            if column <= row {
                let row_begining = row - column;
                return (row_begining..self.height).map(|i| (i - row_begining) * self.height + i).collect();
            }
            else {
                let column_begining = column - row;
                return (column_begining..self.width).map(|i| i * self.height + i - column_begining).collect();
            }
        }
        else {
            if column <= self.height - 1 - row {
                let column_end = column + row;
                return (0..(column_end+1)).map(|i| i * self.height + (column_end - i)).collect();
            }
            else {
                let column_begining = column - (self.height - 1 - row);
                return (column_begining..self.width).map(|i| i * self.height + (self.height - 1 + column_begining - i)).collect();
            }
        }
    }

    fn check_connection(&self, indices: Vec<usize>, condition: impl Fn(&Cell) -> bool) -> bool {
        let mut counter: i32 = 0;
        for i in indices {
            match condition(&self.cells[i]) {
                true => counter += 1,
                false => counter = 0,
            }
            if counter == 4 {
                return true;
            }
        }
        false
    }

    fn is_winning(&self, action: (usize, usize)) -> bool {
        let (column, row) = action;
        let player_color: Color;
        
        match &self.cells[column * self.height + row] {
            Cell::Occupied(color) => match color {
                Color::Red => player_color = Color::Red,
                Color::Blue => player_color = Color::Blue,
            },
            Cell::Empty => panic!("Invalid action {:?} passed to Board::is_winning", action),
        }

        let condition = |cell: &Cell| -> bool {
            match cell {
                Cell::Empty => false,
                Cell::Occupied(color) => match (color, &player_color) {
                    (Color::Blue, Color::Blue) => true,
                    (Color::Red, Color::Red) => true,
                    _ => false,
                },
            }
        };

        let column_indices = self.get_column_indices(column);
        if self.check_connection(column_indices, condition) {
            return true;
        }
        
        let row_indices = self.get_row_indices(row);
        if self.check_connection(row_indices, condition) {
            return true;
        }

        let diagonal_up_indices = self.get_diagonal_indices(column, row, true);
        if self.check_connection(diagonal_up_indices, condition) {
            return true;
        }

        let diagonal_down_indices = self.get_diagonal_indices(column, row, false);
        if self.check_connection(diagonal_down_indices, condition) {
            return true;
        }

        false
    }

    fn eog(&self) -> bool {
        if self.is_full() {
            return true;
        }

        for column in 0..self.width {
            let row = self.get_column_height(column);
            if row > 0 {
                if self.is_winning((column, row - 1)) {
                    return true;
                }
            }
        }
        
        false
    }

    fn play(&mut self, column: usize) -> Option<usize> {
        let row = self.get_column_height(column);

        if row != self.height {
            if self.number_tokens % 2 == 0 {
               self.cells[column * self.height + row] = Cell::Occupied(Color::Red);
            }
            else {
                self.cells[column * self.height + row] = Cell::Occupied(Color::Blue);
            }
            self.number_tokens += 1;
            return Some(row);
        }
        
        //panic!("{} is not a valid move, consider using Board::get_possible_columns() to get a list of valid columns", column);
        return None;
    }

    fn display(&self) {
        println!("{}", "-".repeat(self.width*2 + 1));
                 
        for row in (0..self.height).rev() {
            print!(" ");
            for column in 0..self.width {
                match &self.cells[column * self.height + row] {
                    Cell::Empty => print!(". "),
                    Cell::Occupied(color) => match color {
                        Color::Blue => print!("O "),
                        Color::Red => print!("X "),
                    },
                }
            }
            print!("\n")
        }
        
        println!("{}", "-".repeat(self.width*2 + 1));
    }
}

trait Player {
    fn choose_action(&self, _: &Board) -> usize;
    fn print_eog_message(&self); 
}

struct AIPlayer {
    color: Color,
    depth: u32,
    eog_message: String,
}

impl AIPlayer {
    fn new(color: Color, depth: u32, eog_message: String) -> Self {
        Self { color: color, depth: depth, eog_message: eog_message }
    }

    fn rate_move(&self, board: &Board, column: usize) -> u32 {
        let mut result: u32 = 0;
        let row = board.get_column_height(column);
        
        let column_indices = board.get_column_indices(column);
        for i in (0..row).rev() {
           if let Cell::Occupied(color) = board.get_cell(column_indices[i]) {
               if color == &self.color {
                   result += 1;
               }
           }
        }
        result += <usize as TryInto<u32>>::try_into(column_indices.len() - row + 1).unwrap();
        if result < 4 {
            result = 0;
        }
        else {
            result -= 4;
        }

        let row_indices = board.get_row_indices(row);
        let mut start: usize = 0;
        let mut end = column + 1;
        if column >= 4 {
            start = column - 3;
            end = row_indices.len() - 4;
        }
        'out: for i in start..end {
            for k in 0..4 {
                if let Cell::Occupied(color) = board.get_cell(row_indices[i + k]) {
                    if color != &self.color {
                        continue 'out;
                    }
                }
                if k == 3 {
                    result += 1;
                }
            }
        }

        let up_diag_indices = board.get_diagonal_indices(column, row, true);
        if up_diag_indices.len() >= 4 {
            let pos = cmp::min(column, row);
            if pos >= 4 {
                start = pos - 3;
            }
            else {
                start = 0;
            }
            if up_diag_indices.len() - pos <= 3 {
                end = up_diag_indices.len()-3;
            }
            else {
                end = pos + 1;
            }
            'out: for i in start..end {
                for k in 0..4 {
                    if let Cell::Occupied(color) = board.get_cell(up_diag_indices[i + k]) {
                        if color != &self.color {
                            continue 'out;
                        }
                    }
                    if k == 3 {
                        result += 1;
                    }
                }
            }
        }

        let down_diag_indices = board.get_diagonal_indices(column, row, false);
        if down_diag_indices.len() >= 4 {
            let pos = cmp::min(column, 6 - row - 1);
            if pos >= 4 {
                start = pos - 3;
            }
            else {
                start = 0;
            }
            if down_diag_indices.len() - pos <= 3 {
                end = down_diag_indices.len() - 3;
            }
            else {
                end = pos + 1;
            }
            'out: for i in start..end {
                for k in 0..4 {
                    if let Cell::Occupied(color) = board.get_cell(down_diag_indices[i + k]) {
                        if color != &self.color {
                            continue 'out;
                        }
                    }
                    if k == 3 {
                        result += 1;
                    }
                }
            }
        }
        
        result
    }

    fn heuristic(&self, board: &Board) -> f32 {
        let grid = [
            3.0, 4.0, 5.0, 5.0, 4.0, 3.0,
            4.0, 6.0, 8.0, 8.0, 6.0, 4.0,
            5.0, 8.0, 11.0, 11.0, 8.0, 5.0,
            7.0, 10.0, 13.0, 13.0, 10.0, 7.0,
            5.0, 8.0, 11.0, 11.0, 8.0, 5.0,
            4.0, 6.0, 8.0, 8.0, 6.0, 4.0,
            3.0, 4.0, 5.0, 5.0, 4.0, 3.0
        ];

        let mut result: f32 = 0.0;

        for i in 0..(6*7) {
            if let Cell::Occupied(cell_color) = board.get_cell(i) {
                if cell_color == &self.color {
                    result += grid[i];
                }
                else {
                    result -= grid[i];
                }
            }
        }

        result
    }

    fn alpha_beta(&self, board: &Board, mut alpha: f32, mut beta: f32, depth: u32) -> f32 {
        if board.is_full() {
            return 0.0;
        }
        if depth == 0 {
            return self.heuristic(board);
        }

        let mut v: f32;
        if (self.depth - depth) % 2 == 1 {
            v = f32::INFINITY;
            let mut possible_columns = board.get_possible_columns();
            possible_columns.sort_by(|a, b| (4 - ((*a as i32) - 3).abs()).cmp(&(4 - ((*b as i32) - 3).abs())));
            for column in possible_columns {
                let mut new_board = board.clone();
                let row = new_board.play(column).unwrap();
                if new_board.is_winning((column, row)) {
                    //println!("{}{}","\t".repeat((self.depth-depth).try_into().unwrap()), 100000.0);
                    return -100000.0;
                }
                v = v.min(self.alpha_beta(&new_board, alpha, beta, depth - 1));
                //println!("{}{}","\t".repeat((self.depth-depth).try_into().unwrap()), v);
                if alpha >= v {
                    return v;
                }
                beta = beta.min(v);
            }
        }
        else {
            v = -f32::INFINITY;
            let mut possible_columns: Vec<(u32, usize)> = board.get_possible_columns()
                                                                .iter()
                                                                .map(|column| (self.rate_move(board, *column), *column))
                                                                .collect::<Vec<_>>();
            possible_columns.sort_by(|(s1, c1), (s2, c2)| (s1, 4 - ((*c1 as i32) - 3).abs()).cmp(&(s2, 4 - ((*c2 as i32) - 3).abs())));
            for (_, column) in possible_columns.into_iter().rev() {
                let mut new_board = board.clone();
                let row = new_board.play(column).unwrap();
                if new_board.is_winning((column, row)) {
                    //println!("{}{}","\t".repeat((self.depth-depth).try_into().unwrap()), -100000.0);
                    return 100000.0;
                }
                v = v.max(self.alpha_beta(&new_board, alpha, beta, depth - 1));
                //println!("{}{}","\t".repeat((self.depth-depth).try_into().unwrap()), v);
                if v >= beta {
                    return v;
                }
                alpha = alpha.max(v);
            }
        }

        return v;
    }
}

impl Player for AIPlayer {
    fn choose_action(&self, board: &Board) -> usize {
        let mut best_action = board.get_possible_columns()[0];
        let mut best_score = -f32::INFINITY;

        let mut possible_columns: Vec<(u32, usize)> = board.get_possible_columns()
                                                            .iter()
                                                            .map(|column| (self.rate_move(board, *column), *column))
                                                            .collect::<Vec<_>>();
        possible_columns.sort_by(|(s1, c1), (s2, c2)| (s1, 4 - ((*c1 as i32) - 3).abs()).cmp(&(s2, 4 - ((*c2 as i32) - 3).abs())));
        //println!("{:?}", possible_columns);
        for (_, column) in possible_columns.into_iter().rev() {
            let mut new_board = board.clone();
            let row = new_board.play(column).unwrap();
            if new_board.is_winning((column, row)) {
                return column;
            }
            let score = self.alpha_beta(&new_board, -f32::INFINITY, f32::INFINITY, self.depth - 1);
            //println!("{}, {}", column, score);
            if score > best_score {
                best_score = score;
                best_action = column;
            }
        }

        best_action
    }
    
    fn print_eog_message(&self) {
        println!("{}", self.eog_message)
    }
}

struct HumanPlayer {
    color: Color,
    eog_message: String,
}

impl HumanPlayer {
    fn new(color: Color, eog_message: String) -> Self {
        Self { color: color, eog_message: eog_message }
    }
}

impl Player for HumanPlayer {
    fn choose_action(&self, board: &Board) -> usize {
        loop {
            println!("Choose a column (1 to 7):");
            let mut input_text = String::new();
            io::stdin()
                .read_line(&mut input_text)
                .expect("failed to read from stdin");

            let trimmed = input_text.trim();
            match trimmed.parse::<usize>() {
                Ok(i) => {
                    if i > 0 && board.get_possible_columns().contains(&(i - 1)) {
                        return i - 1;
                    }
                    else {
                        println!("you selected an invalid column");
                    }
                },
                Err(..) => println!("this was not an integer: {}", trimmed),
            }
        }
    }
    
    fn print_eog_message(&self) {
        println!("{}", self.eog_message)
    }
}

fn play_game<'a>(mut player_1: &'a dyn Player, mut player_2: &'a dyn Player) {
    let mut board = Board::new();
    
    while !board.eog() {
        let action = player_1.choose_action(&board);
        board.play(action);
        board.display();
        let tmp = player_1;
        player_1 = player_2;
        player_2 = tmp;
    }

    println!("============== END =============");
    board.display();
    player_2.print_eog_message();
}

fn main() {
    let player_1 = HumanPlayer::new(Color::Red, "You won! Well done".to_string());
    //let player_1 = AIPlayer::new(Color::Red, 8, "You won!".to_string());
    let player_2 = AIPlayer::new(Color::Blue, 7, "You lost :(".to_string());
    
    play_game(&player_1, &player_2);
    
    //let mut board = Board::new();
    //board.play(3);
    //let results: Vec<u32> = (0..7).map(|i| player_2.rate_move(&board, i)).collect();
    //println!("{:?}", results);
    //board.display();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_possible_columns() {
        let mut board = Board::new();
        for _ in 0..6 {
            board.play(0);
            board.play(2);
            board.play(3);
        }
        board.play(5);
        board.play(6);
        board.play(6);
        assert_eq!(board.get_possible_columns(), vec![1, 4, 5, 6]);
    }

    #[test]
    fn test_diagonal_indices() {
        let board = Board::new();
        assert_eq!(board.get_diagonal_indices(0, 0, true), vec![0, 7, 14, 21, 28, 35]);
        assert_eq!(board.get_diagonal_indices(0, 0, false), vec![0]);
        assert_eq!(board.get_diagonal_indices(3, 3, true), vec![0, 7, 14, 21, 28, 35]);
        assert_eq!(board.get_diagonal_indices(3, 3, false), vec![11, 16, 21, 26, 31, 36]);
        assert_eq!(board.get_diagonal_indices(0, 5, true), vec![5]);
        assert_eq!(board.get_diagonal_indices(0, 5, false), vec![5, 10, 15, 20, 25, 30]);
    }

    #[test]
    fn test_full_board() {
        let mut board = Board::new();
        for column in 0..7 {
            for _ in 0..6 {
                board.play(column);
            }
        }
        assert!(board.is_full());
    }
}
